const YAML = require('yamljs');
const fetchUrl = require("fetch").fetchUrl;
const line = require('@line/bot-sdk');
const express = require('express');

const config = {
  channelAccessToken: 'NklX0C4tnjJrznn/fji5inSmJmOHQsfZ+GCirfDYTtQ1HEPNAY+5UZDkTOsx7q6/rFCmzX0kPF2fl1LruJ7/f2jKT5Gy8kpwVJiDJny0+PTQ2WAKVqBMJAHMxjTrbLupcsqpz4/NnysYEjbfY7kQ4AdB04t89/1O/w1cDnyilFU=',
  channelSecret: 'ad4c6a706a5006dd8621a1ecb0c9bad1',
};

const client = new line.Client(config);
const app = express();

let dummyResponse = {};

const fetchData = () => {
  const time = (new Date()).getTime();
  fetchUrl('https://raw.githubusercontent.com/madya121/brobro-dataset/master/dataset.yml?time=' + time, (err, meta, data) => {
    dummyResponse = YAML.parse(data.toString());
    console.log('Data Updated');
  });
}
fetchData();

app.get('/update', (req, res) => {
  fetchData();
  res.send('Updating Data...');
})

app.post('/', line.middleware(config), (req, res) => {
  Promise
    .all(req.body.events.map(handleEvent))
    .then((result) => res.json(result))
    .catch((err) => {
      console.error(err);
      res.status(500).end();
    });
});

app.get('/', (req, res) => {
  res.send(YAML.load(''));
});

const getPlesetan = (sentence) => {
  if (!sentence || sentence.length >= 150) {
    return null;
  }
  
  let ret = null;
  const arr = sentence.split(' ');
  
  arr.forEach((word) => {
    const p = dummyResponse[word.toUpperCase()];
    if (p) {
      const time = (new Date()).getTime();
      ret = p[time % p.length];
    }
  });
  
  return ret;
}

const handleEvent = (event) => {
  if (event.type !== 'message' || event.message.type !== 'text') {
    return Promise.resolve(null);
  }

  const plesetan = getPlesetan(event.message.text);
  
  if (!plesetan)
    return;
  
  const resp = { type: 'text', text: plesetan };

  return client.replyMessage(event.replyToken, resp);
}

const port = 8080;
app.listen(port, () => {
  console.log(`listening on ${port}`);
});